1.Algorithm：每周至少做一个 leetcode 的算法题

2.Review：阅读并点评至少一篇英文技术文章

3.Tip：学习至少一个技术技巧

4.Share：分享一篇有观点和思考的技术文章

1.算法：https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/submissions/
class Solution {
    public int[] searchRange(int[] nums, int target) {
        int[] result = {-1, -1};
        if (nums == null || nums.length == 0){
            return result;
        }
        int length = nums.length;
        int start = 0;
        int end = length;
        if (nums[0] == target && nums[length - 1] == target) {
            result[0] = 0;
            result[1] = length - 1;
            return result;
        }

        for (int i = start; i < end; i++) {
            int index = ((end - start) >> 1) + start;
            if (nums[index] > target) {
                end = index;
                i=start-1;
            } else if (nums[index] < target) {
                start = index + 1;
                i = index;
            } else {
                for (int j = index-1; j >= 0; j--) {
                    if (nums[j] < target) {
                        result[0] = j + 1;
                        break;
                    }
                }
                if (result[0] == -1) {
                    result[0] = 0;
                }
                for (int j = index+1; j < length; j++) {
                    if (nums[j] > target) {
                        result[1] = j - 1;
                        break;
                    }
                }
                if (result[1] == -1) {
                    result[1] = length - 1;
                }

                return result;
            }
        }
        return result;
    }
}
    
2.https://readwrite.com/2019/08/15/3-programming-languages-offering-better-support-for-iot-development/
3 Programming Languages Offering Better Support for IoT Development
Java,python,php，这3者是用于物联网开发的合适语言


3. 继续学习python：
使用type()可以用来判断对象类型，比如type(123)，输出结果是：<class 'int'>
判断一个对象是否是函数可以使用types模块中定义的常量：
import types
type(abs)==types.BuiltinFunctionType
得到结果为True
使用isinstance()可以判断class的继承关系
使用dir()函数可以获得一个对象的所有属性和方法
关于实例属性和类属性：
类的属性，所有的实例都可以访问；
实例属性可以覆盖同名的类属性，但通过类名.属性还是可以访问类属性的
用del方法删除实例属性后，再通过实例访问属性，得到的是类的属性
使用__slots__可以限制一个类的属性
                                                       
4.https://juejin.im/post/5b7593496fb9a009b62904fa#comment
你应该知道的缓存进化史