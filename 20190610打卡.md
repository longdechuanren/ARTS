1.Algorithm：每周至少做一个 leetcode 的算法题

2.Review：阅读并点评至少一篇英文技术文章

3.Tip：学习至少一个技术技巧

4.Share：分享一篇有观点和思考的技术文章

1.算法：
https://leetcode.com/problems/range-sum-of-bst/

class Solution {
    public int rangeSumBST(TreeNode root, int L, int R) {
        int total = 0;
        if (root != null){
            TreeNode rt = root;
            Stack<TreeNode> stack = new Stack<>();

            while (rt != null || !stack.empty()) {
                while (rt != null) {
                    stack.push(rt);
                    rt = rt.left;
                }

                TreeNode tp = stack.pop();
                if (tp.val>=L && tp.val<=R){
                    total += tp.val;
                }
                rt = tp.right;
            }
        }
        return total;
    }
}

2. https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-learn-new-technology-in-a-corporate-environment
How to learn new technology in a corporate environment
在公司环境如何学习新技术，作者讲述了培训和教育的区别，指出应该给员工足够的时间学习新技术，并且要不断练习、实践，不要集中式的一周培训，培训完又不用。

3.java8的Optional类学习
of，ofNullable，isPresent，map等方法的使用。

4.Spring中如何使用责任链模式
https://my.oschina.net/zhangxufeng/blog/3055328